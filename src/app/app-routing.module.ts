import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './catalog/products/products.component';
import { SigninComponent } from './authentication/signin/signin.component';
import { RegisterComponent } from './authentication/register/register.component';
import { WelcomeComponent } from './authentication/welcome/welcome.component';

const routes: Routes = [
    { path:'', component:ProductsComponent },
    { path:'signin', component:SigninComponent },
    { path:'register', component:RegisterComponent },
    { path:'welcome', component:WelcomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
