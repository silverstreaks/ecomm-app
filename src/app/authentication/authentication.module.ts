import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { RegisterComponent } from './register/register.component';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ProfileComponent } from './profile/profile.component';
import { WelcomeComponent } from './welcome/welcome.component';
@NgModule({
  declarations: [SigninComponent, RegisterComponent, ConfirmationComponent, ProfileComponent, WelcomeComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule
  ],
  exports:[
    SigninComponent,
    RegisterComponent,
    WelcomeComponent,
    ProfileComponent,
    ConfirmationComponent
  ]
})
export class AuthenticationModule { }
