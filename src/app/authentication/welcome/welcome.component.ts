import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  name:string = '';
  constructor(private route:ActivatedRoute) {
    this.route
        .queryParams
        .subscribe(params => {
            this.name = params['name'];
            let email = params['email'];
        });
  }

  ngOnInit() {
  }

}
