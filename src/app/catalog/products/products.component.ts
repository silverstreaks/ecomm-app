import { Component, OnInit } from '@angular/core';
import { Product } from '../domain/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products:any[];

  constructor() {
    this.products = [];
    this.products.push(new Product(1,'The secret', 'Wonderfull book, you must try',254.20));
    this.products.push(new Product(2,'Again a secret', 'Exceiting book, with limmited period offer',25));
   }

  ngOnInit() {
  }

}
