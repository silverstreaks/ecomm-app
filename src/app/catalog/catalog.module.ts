import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  declarations: [ProductsComponent],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports:[
    ProductsComponent
  ]
})
export class CatalogModule { }
