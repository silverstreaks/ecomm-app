
export class Product{
    id: number;
    name: string;
    description: string;
    price: number;

    constructor(theId: number, theName: string, theDescription: string, thePrice: number){
        this.id = theId;
        this.name = theName;
        this.description = theDescription;
        this.price = thePrice
    }

}